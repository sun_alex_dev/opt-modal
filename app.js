(function () {
  "use strict";

  //App namespace
  var Api = {
    Modal     : null,
    Controller: {},
    Utils     : {}
  };

  Api.Utils = (function () {

    var Utils = {};

    /**
     * Extract data from html control
     * @param {Element} elem
     * @returns {number|string|boolean}
     */
    Utils.formSerialize = function (elem) {
      switch (elem.type) {
        case 'checkbox':
          return elem.checked;
        case 'number':
          return (elem.value) ? parseFloat(elem.value) : null;
        default:
          return elem.value;
      }
    };

    return Utils;

  })();

  /**
   * Modal service, instantiate and handle modal events.
   */
  Api.Modal = (function () {

    //Init service object
    var modal = {};

    var _backdrop = null;
    var _modalElem = null;

    var _isOpened = false;

    var _onSaveCallback = null;

    modal.open = function (options) {
      if (_isOpened) {
        throw new Error('A modal is already opened');
      }

      _render(options.template);
      _bindController(options.controller);
      _backdropListner();
      _bindCloseListner();

      _isOpened = true;
      return this;
    };

    modal.save = function (data) {
      _onSaveCallback(data);
      _destroy();
    };

    modal.close = function () {
      _destroy();
    };

    modal.onSave = function (callback) {
      _onSaveCallback = callback;
    };

    function _createModalElems() {
      _modalElem = document.createElement('div');
      _modalElem.className += 'modal';

      _backdrop = document.createElement('div');
      _backdrop.className += 'backdrop';
    }

    function _render(template) {
      _createModalElems();

      var modalContent = document.querySelector(template);
      _modalElem.innerHTML = modalContent.innerHTML;

      _backdrop.appendChild(_modalElem);
      document.body.appendChild(_backdrop);
    }

    function _bindController(controller) {
      controller.setView(_modalElem);
      controller.init();
    }

    /**
     * Bind event listener to close modal button from header
     * @private
     */
    function _bindCloseListner() {
      _modalElem.querySelector('.modal__close-btn')
        .addEventListener('click', modal.close);
    }

    function _backdropListner() {
      window.addEventListener('click', function (e) {
        if (e.target.className.indexOf('backdrop') !== -1) {
          modal.close();
        }
      });
    }

    function _destroy() {
      document.body.removeChild(_backdrop);
      _isOpened = false;
      _onSaveCallback = null;
      window.removeEventListener('click', _backdropListner);
    }

    return modal;
  })();


  /**
   * This is the validation service, parse the model
   * and the model meta to check if all the constraints
   * from the meta are applied.
   */
  Api.Validator = (function () {

    var Validator = {};

    function _required(value) {
      return !!(value);
    }

    function _number(value) {
      return (typeof value === 'number');
    }

    function _min(value, ln) {
      return (value >= ln );
    }

    function _max(value, ln) {
      return (value <= ln);
    }

    function _hex(value) {
      var regExp = /^#([0-9a-f]{6})$/i;
      return regExp.test(value);
    }

    function _minln(value, ln) {
      return (value.length >= ln);
    }

    function _maxln(value, ln) {
      return (value.length <= ln);
    }

    var _contraints = {
      required: {
        isValid: _required,
        message: function () {
          return 'This field is required'
        }
      },
      number  : {
        isValid: _number,
        message: function () {
          return 'This is not a valid number'
        }
      },
      min     : {
        isValid: _min,
        message: function (arg) {
          return 'This field must be greater than ' + arg;
        }
      },
      max     : {
        isValid: _max,
        message: function (arg) {
          return 'This fiels must be lower than ' + arg;
        }
      },
      hex     : {
        isValid: _hex,
        message: function () {
          return 'This field is not a valid HEX';
        }
      },
      minlen  : {
        isValid: _minln,
        message: function (arg) {
          return 'This field must have more than ' + arg + ' characters';
        }
      },
      maxlen  : {
        isValid: _maxln,
        message: function (arg) {
          return 'This field must have less than ' + arg + ' characters';
        }
      }
    };

    Validator.validateField = function (value, rules, field) {
      var param, valid, response = {};
      response[field] = {messages: []};

      if (Object.keys(rules).length === 0) return false;

      for (var rule in rules) {
        param = rules[rule];
        valid = _contraints[rule].isValid(value, param);
        if (!valid) {
          response[field].messages.push(_contraints[rule].message(param))

          if (rule === 'required') {
            response[field].messages = [_contraints[rule].message(param)];
            break;
          }
        }
      }

      if (response[field].messages.length > 0) return response[field];

      return false;
    };

    return Validator;

  })();


  /**
   * Modal model with the car info.
   */
  Api.Model = (function () {

    var Model = function () {
      this.storage = {};

      // Validation metadata
      this.validation = {
        Title      : {required: true, minlen: 3, maxlen: 15},
        Brand      : {required: true},
        Year       : {required: true, number: true, min: 1960, max: (new Date()).getFullYear()},
        Mileage    : {required: true},
        Fuel       : {required: true},
        Color      : {required: true, hex: true},
        Damaged    : {},
        Reason     : {},
        Price      : {required: true, number: true},
        Description: {},
        Currency   : {}
      };
    };

    var _errors = {};

    Model.prototype.set = function (key, value) {
      var errors = Api.Validator.validateField(value, this.validation[key], key);
      if (errors) {
        _errors[key] = errors;
        return errors;
      } else {
        delete _errors[key];
      }

      this.storage[key] = value;
    };

    Model.prototype.hasErrors = function () {
      return Object.keys(_errors).length > 0;
    };

    Model.prototype.toJSON = function () {
      return this.storage;
    };

    return Model;
  })();


  Api.Toast = (function () {

    var Toast = {};

    var elem = document.querySelector('.toast');
    var messageBox = elem.querySelector('.toast__message');
    var show = false;

    var hideDealay = 3000;

    Toast.show = function (message) {
      elem.className = 'toast toast--show';
      messageBox.innerHTML = message;

      if (show) return false;
      setTimeout(function () {
        elem.className = 'toast';
      }, hideDealay);
    };

    return Toast;
  })();

  /**
   * This controller is used by tabs widget
   */
  Api.Controller.Tabs = (function () {

    var Tabs = function (selector) {
      this.elem = document.querySelector(selector);
      this.controls = document.querySelectorAll('.tab-menu__control');
      this.tabs = document.querySelectorAll('.tab');
    };

    Tabs.prototype.init = function () {
      var self = this;
      Array.prototype.forEach.call(this.controls, function (control) {
        control.addEventListener('click', self.listenChange.bind(self));
      });
    };

    Tabs.prototype.listenChange = function (e) {
      e.preventDefault();
      var elem = e.target;
      var active = elem.getAttribute('data-tab');

      Array.prototype.forEach.call(this.controls, function (control) {
        control.className = 'tab-menu__control';
      });
      elem.className = 'tab-menu__control tab-menu__control--active';

      Array.prototype.forEach.call(this.tabs, function (tab) {
        if (tab.getAttribute('id') === active) {
          tab.className = 'tab tab--active';
        } else {
          tab.className = 'tab';
        }
      });
    };

    return Tabs;

  })();


  /**
   * This controller is used by Add Car modal
   */
  Api.Controller.ModalController = (function () {

    var _displayDebounce = null;

    var Controller = function () {
      this.elem = null;
      this.model = null;
    };

    Controller.prototype.setModel = function (model) {
      this.model = model;
    };

    Controller.prototype.setView = function (elem) {
      this.elem = elem;
    };

    Controller.prototype.init = function () {
      this.bindEvents();
      this.bindModel();
      this.tabs = new Api.Controller.Tabs('#modal-tabs');
      this.tabs.init();
      this.saveWarn = this.elem.querySelector('.btn-warn__message');
    };

    /**
     * Bind events to the modal "management controls" to achieve
     * different types of behaviors live: close, expand or for
     * sync between controls.
     */
    Controller.prototype.bindEvents = function () {
      this.elem.querySelector('.save-btn')
        .addEventListener('click', this.save.bind(this));

      this.elem.querySelector('.close-btn')
        .addEventListener('click', this.close.bind(this));

      this.elem.querySelector('.vehicle-damaged')
        .addEventListener('change', this.checkDamaged.bind(this));

      this.bindColorPickerEvents();
    };

    /**
     * Bind the html input to the model
     */
    Controller.prototype.bindModel = function () {
      this.controls = this.elem.querySelectorAll('[data-model]');
      var self = this;

      Array.prototype.forEach.call(this.controls, function (model) {
        model.addEventListener('blur', _handleInputChange.bind(self));
      });
    };

    /**
     * Handle 'Vehicle damaged' input changes, when is set to
     * yes toggle 'reason field'
     * @param {Event} e
     */
    Controller.prototype.checkDamaged = function (e) {
      var elem = e.target;
      var form = this.elem.querySelector('.damaged-reason-form');
      if (elem.checked) {
        form.style.display = 'block';
      } else {
        form.style.display = 'none';
      }
    };

    /**
     * Bind events on color input and color picker to keep them
     * in sync, when one of them is changed.
     */
    Controller.prototype.bindColorPickerEvents = function () {
      var htmlInput = this.elem.querySelector('.car-color');
      var input = this.elem.querySelector('input[data-model="Color"]');

      htmlInput.addEventListener('change', function (e) {
        input.value = e.target.value;
      });

      input.addEventListener('blur', function (e) {
        htmlInput.value = e.target.value;
      });
    };

    Controller.prototype.serializeForm = function () {
      var self = this;
      Array.prototype.forEach.call(this.controls, function (control) {
        self.parseControl(control);
      });
    };

    /**
     * Display an error message above the save button when the
     * form don't pass the validation
     */
    Controller.prototype.displayErrorsWarning = function (hasErrors) {
      var self = this;

      if (hasErrors) {
        this.saveWarn.style.display = 'inline-block';


        //Prevent the message when the save btn was clicked
        clearTimeout(_displayDebounce);
        _displayDebounce = setTimeout(function () {
          self.saveWarn.style.display = 'none';
        }, 3000);
      }
    };

    /**
     * Flash all the data from modal model to the
     * main controller callback
     */
    Controller.prototype.save = function () {
      this.serializeForm();
      if (this.model.hasErrors()) {

        this.displayErrorsWarning(true);
      } else {
        Api.Modal.save(this.model.toJSON());
      }
    };

    /**
     * Close the modal without sending data.
     */
    Controller.prototype.close = function () {
      Api.Modal.close();
    };

    /**
     * Extract and set data from the control into the model,
     * if the model is invalid append the errors in the view.
     * @param {Element} elem
     */
    Controller.prototype.parseControl = function (elem) {
      var container = elem.parentNode;

      var field = elem.getAttribute('data-model');
      var value = Api.Utils.formSerialize(elem);

      var errors = this.model.set(field, value);

      var errElem = container.querySelector('.errors');
      if (errElem) container.removeChild(errElem);

      if (errors) {
        var el = _renderErrors(errors.messages);
        container.appendChild(el);
      }
    };

    /**
     * Build an errors element from the model field errors.
     * @param {Array} errors
     * @returns {Element}
     * @private
     */
    function _renderErrors(errors) {
      var container = document.createElement('div');
      container.className = 'errors';

      var errorField = null;
      errors.forEach(function (error) {
        errorField = document.createElement('p');
        errorField.className = 'errors__message';
        errorField.appendChild(document.createTextNode(error));

        container.appendChild(errorField);
      });

      return container;
    }

    /**
     * This function is bind as listener for the blur
     * events from the view to set the data in the model.
     * @param {Event} e
     * @private
     */
    function _handleInputChange(e) {
      this.parseControl(e.target);
    }

    return Controller;

  })();

  Api.Controller.MainView = (function () {
    var Controller = function (selector) {
      this.elem = document.querySelector(selector);
    };

    Controller.prototype.init = function () {
      this.table = this.elem.querySelector('.table');
      this.warningText = this.elem.querySelector('.no-products-warning');
    };

    Controller.prototype.buildRow = function (datum) {
      var tr = document.createElement('tr');

      var title = _createTd(datum.Title);
      var price = _createTd(datum.Price);
      var year = _createTd(datum.Year);
      var mark = _createMarkTd(datum.Brand);

      tr.appendChild(title);
      tr.appendChild(price);
      tr.appendChild(year);
      tr.appendChild(mark);

      return tr;
    };

    Controller.prototype.renderData = function (data) {
      this.warningText.style.display = 'none';
      this.table.style.display = 'table';

      var container = this.elem.querySelector('#products-container');
      this.table = container.removeChild(this.table);

      // Remove all rows
      var tBody = this.table.querySelector('tbody');
      this.table.removeChild(tBody);
      tBody = document.createElement('tbody');

      // Build table body
      var self = this;
      data.forEach(function (datum) {
        var tr = self.buildRow(datum);
        tBody.appendChild(tr);
      });

      // Append table
      this.table.appendChild(tBody);
      container.appendChild(this.table);
    };

    function _createMarkTd(mark) {
      var td = document.createElement('td');
      var img = document.createElement('img');

      switch (mark) {
        case 'BMW':
          img.setAttribute('src', 'img/bmw-logo.png');
          img.setAttribute('alt', 'bmw logo');
          break;
        case 'Audi':
          img.setAttribute('src', 'img/audi-logo.png');
          img.setAttribute('alt', 'audi logo');
          break;
        case 'Opel':
          img.setAttribute('src', 'img/opel-logo.png');
          img.setAttribute('alt', 'opel logo');
          break;
      }

      td.appendChild(img);
      return td;
    }

    function _createTd(text) {
      var elem = document.createElement('td');
      var _text = document.createTextNode(text);
      elem.appendChild(_text);
      return elem;
    }

    return Controller;

  })();

  /**
   * Bootstrap the application
   */
  (function () {

    var addCarBtn = document.querySelector('.add-car-btn');
    addCarBtn.addEventListener('click', handleModal);

    var MainViewController = new Api.Controller.MainView('.main-content');
    MainViewController.init();

    var products = [];

    /**
     * Handler for add button, open the 'Add Product' modal
     */
    function handleModal() {
      var controller = new Api.Controller.ModalController;
      var model = new Api.Model;

      controller.setModel(model);

      Api.Modal.open({
        template  : '#car-modal-template',
        controller: controller
      }).onSave(function (response) {
        products.push(response);
        MainViewController.renderData(products);
        Api.Toast.show('Product saved with success!');
      });
    }

  })();

})();
